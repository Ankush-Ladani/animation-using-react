import { useEffect, useState } from "react";
import "./App.css";
import LeftSection from "./components/LeftSection";
import RightSection from "./components/RightSection";
import TimePass from "./components/TimePass";

function App() {
  const [scrollPosition, setScrollPosition] = useState(0);

  const handleScroll = () => {
    const position = window.pageYOffset;
    setScrollPosition(position);
  };

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  });

  return (
    <>
      <div className="App">
        <LeftSection />
        <RightSection scrollPosition={scrollPosition} />
      </div>
      <TimePass />
      <TimePass />
    </>
  );
}

export default App;
