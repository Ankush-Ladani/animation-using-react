import React, { useEffect, useState } from "react";

const LeftSection = () => {
  return (
    <div>
      <div className="leftSection">
        <span>Wordpress</span>
        <h1>Industry leading website builder</h1>
        <p>
          WordPress is an open-source website builder and content management
          system suitable for small and large-scale businesses.
        </p>
        <p>UX/UI Design, Marketing Materials</p>
        <a href="https://arounda.agency/case/wordpress">VIEW PROJECT</a>
      </div>
      <div className="leftSection">
        <span>Wordpress</span>
        <h1>Industry leading website builder</h1>
        <p>
          WordPress is an open-source website builder and content management
          system suitable for small and large-scale businesses.
        </p>
        <p>UX/UI Design, Marketing Materials</p>
        <a href="https://arounda.agency/case/wordpress">VIEW PROJECT</a>
      </div>
      <div className="leftSection">
        <span>Wordpress</span>
        <h1>Industry leading website builder</h1>
        <p>
          WordPress is an open-source website builder and content management
          system suitable for small and large-scale businesses.
        </p>
        <p>UX/UI Design, Marketing Materials</p>
        <a href="https://arounda.agency/case/wordpress">VIEW PROJECT</a>
      </div>
      <div className="leftSection">
        <span>Wordpress</span>
        <h1>Industry leading website builder</h1>
        <p>
          WordPress is an open-source website builder and content management
          system suitable for small and large-scale businesses.
        </p>
        <p>UX/UI Design, Marketing Materials</p>
        <a href="https://arounda.agency/case/wordpress">VIEW PROJECT</a>
      </div>
      <div className="leftSection">
        <span>Wordpress</span>
        <h1>Industry leading website builder</h1>
        <p>
          WordPress is an open-source website builder and content management
          system suitable for small and large-scale businesses.
        </p>
        <p>UX/UI Design, Marketing Materials</p>
        <a href="https://arounda.agency/case/wordpress">VIEW PROJECT</a>
      </div>
    </div>
  );
};

export default LeftSection;
