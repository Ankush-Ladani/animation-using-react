import React, { useEffect, useState } from "react";

const RightSection = ({ scrollPosition }) => {
  // const [colorsArray2, setColorsArray2] = useState(["red"]);

  const [classArray, setClassArray] = useState(["circle"]);

  const [count, setCount] = useState(0);

  const colorsArray = ["red", "blue", "aqua", "gray"];

  // const leaves = `
  // @keyframes react-fade-in {
  //   0%   { opacity: 0; }
  //   50%  { opacity: 0; }
  //   100% { opacity: 1; }
  // }`;

  // const animate = {
  //   animationDuration: "1s",
  //   animationIterationCount: 2,
  //   animationName: `react-fade-${leaves}`,
  //   animationTimingFunction: "ease-in-out",
  //   MozAnimationDirection: "alternate",
  // };

  // const classArray = ["circle"];

  return (
    <div className="rightSection">
      <div
        style={{
          backgroundColor:
            scrollPosition >= 320 && scrollPosition < 900
              ? "yellow"
              : scrollPosition >= 900 && scrollPosition < 1480
              ? colorsArray[0]
              : scrollPosition >= 1480 && scrollPosition < 2060
              ? colorsArray[1]
              : scrollPosition >= 2060 && scrollPosition < 2640
              ? colorsArray[2]
              : scrollPosition >= 2640 && scrollPosition <= 2650
              ? colorsArray[3]
              : "#f0f",
        }}
        className={
          // "circle"
          scrollPosition >= 320 && scrollPosition < 900
            ? "animate"
            : scrollPosition >= 900 && scrollPosition < 1480
            ? "animate2"
            : scrollPosition >= 1480 && scrollPosition < 2060
            ? "animate3"
            : scrollPosition >= 2060 && scrollPosition < 2640
            ? "animate4"
            : scrollPosition >= 2640 && scrollPosition <= 2650
            ? "animate5"
            : [...classArray]
        }>
        .
      </div>
      <div className="imgDiv">
        {/* <img
          src="https://arounda.agency/assets/images/index/wordpress.jpg"
          width="700vw"
          alt=""
        /> */}
      </div>
    </div>
  );
};

export default RightSection;
