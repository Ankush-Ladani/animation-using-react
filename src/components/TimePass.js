import React from "react";

const TimePass = () => {
  return (
    <p>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Rem nobis
      delectus obcaecati esse ut, accusantium blanditiis enim id est culpa
      maxime voluptatum animi quidem. Necessitatibus, recusandae velit minima
      architecto eum in iure tenetur vel labore sit exercitationem libero ex
      illo id, similique magni debitis non neque accusantium hic! Veritatis
      distinctio recusandae error earum facilis esse totam, consequuntur odit
      expedita, nisi ex obcaecati suscipit cumque dolores voluptatibus
      voluptatem quod odio exercitationem modi non et. Fuga praesentium natus
      iste beatae incidunt adipisci eligendi laudantium! Totam, possimus aut
      maxime officia vitae delectus consequuntur quisquam doloribus illum
      officiis vero architecto quam fugit modi maiores ipsam ipsum! Nulla magnam
      cupiditate quo enim dignissimos quidem pariatur, quod quam similique
      laborum consequuntur amet at, ipsam ullam dicta rerum dolore rem a
      mollitia? Blanditiis modi aperiam vero quos unde asperiores fugit esse
      corrupti, fugiat nihil adipisci, aut assumenda recusandae praesentium.
      Animi veritatis reprehenderit delectus non quam laudantium consequatur,
      cumque quaerat totam veniam repudiandae provident? Minima quaerat tempora
      dicta eveniet iure molestias, animi maxime a deleniti maiores voluptatibus
      sequi quasi tenetur enim delectus! Neque, accusantium sit esse
      consequuntur quam iusto quia! Earum beatae blanditiis, vero repudiandae
      sed veniam velit suscipit, commodi aut nam non repellat quia? Recusandae
      reprehenderit beatae repudiandae dolorum odit iure repellendus quam neque
      sunt delectus ullam quo doloribus saepe, sapiente magnam architecto. Nulla
      autem, corporis optio blanditiis atque ullam rerum, veritatis eum soluta
      porro quod ipsum ducimus, voluptatibus officiis doloribus praesentium at
      quae consequatur minima numquam! Doloribus nobis nemo adipisci expedita
      blanditiis saepe illo cumque facere rerum odit, atque placeat amet quasi
      non nisi ut voluptate voluptatum labore et modi aliquid dolore dignissimos
      sint ex. Libero ipsum aliquid quis odit repellendus sapiente! Corrupti
      quas in error et voluptatem praesentium qui fuga natus esse maiores.
      Soluta id quis nulla placeat atque, fugiat, sapiente quisquam maxime
      cumque doloribus possimus qui ducimus odio eos veniam? Cumque dignissimos
      ullam animi consequatur similique adipisci cupiditate reprehenderit quos
      beatae voluptatibus nisi, quam non harum distinctio dolore fugit voluptate
      deleniti nihil sit recusandae nostrum maiores, voluptas excepturi.
      Deleniti, fuga esse? Perspiciatis culpa tempore voluptatibus eius ab quo
      assumenda in blanditiis, distinctio nobis eaque autem nulla vel doloribus
      labore tenetur excepturi ad iusto sequi ea, voluptatum facere ut
      praesentium. Inventore quas quis rerum possimus. Blanditiis culpa quas
      harum saepe doloribus commodi numquam sit facere neque quam? Repudiandae
      voluptatum ut vel ex tenetur, eum excepturi nam deserunt? Repellendus
      blanditiis illo vero eum? Pariatur esse soluta, mollitia, minus autem
      asperiores similique debitis aut nihil consequuntur labore cumque neque
      quo at commodi eveniet expedita repudiandae non eaque. Iure officia
      voluptatem sunt animi. Nostrum pariatur quibusdam, vero itaque, officiis
      ipsam repudiandae numquam omnis ad, odit ullam accusantium sequi nisi
      corporis fugit sit neque non error? Fugit eius cupiditate veniam nostrum,
      sapiente nihil architecto accusantium quidem laudantium voluptatibus dicta
      maxime reiciendis praesentium ipsa quo nulla quos enim dolore consequuntur
      voluptas quibusdam voluptatem, aspernatur neque fuga. Velit dolor nisi
      vitae quas? A facere sint, omnis nisi perspiciatis aliquid at cum nemo
      ipsum, corporis optio necessitatibus laborum vel deleniti libero! Odit!
    </p>
  );
};

export default TimePass;
